﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Behaviour_Rotation : Command
{
    public void Execute(GameObject _target)
    {
        _target.transform.Rotate(Vector3.right * 90);
        _target.transform.Translate(_target.transform.forward,Space.World);
        _target.transform.Translate(_target.transform.up * 0.25f, Space.World);

    }
}
