﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Controller_Actions : MonoBehaviour
{
    private InputHandler inputHandler;
    private Checker checker;
    [SerializeField]
    private bool canMove = true;
    public Action<Rotations> rotateWorld;


    private void Awake()
    {
        checker = GetComponent<Checker>();
        inputHandler = new InputHandler();
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            if (checker.GroundChecker())
            {
                if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                {
                    Command cmd = inputHandler.HandlerInput(Action_Move.Move);
                    cmd.Execute(gameObject);
                }
            }
            else
            {
                {
                    canMove = false;
                    Command cmd = inputHandler.HandlerInput(Action_Move.Rotate);
                    cmd.Execute(gameObject);
                    rotateWorld((checker.RotateDirection(transform)));
                }
            }
        }

    }

    public void ActivateCanMove()
    {
        canMove = true;
    }
}
